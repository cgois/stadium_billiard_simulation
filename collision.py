# This is a simulation for the project "Chaotic Optical Resonators,"
# under research at the Device Research Laboratory (LPD), 
# State University of Campinas, Brazil.
#
# WHAT DOES IT DO?
#   It simulates a stadium billiard system.
#   
#   INPUT:  The rectangle width and height (= semicircle diameter.)
#   
#   OUTPUT: Phase diagram of Arc Length Fraction x Alpha, where 
#           alpha is the reflection angle with respect to the tangent of
#           the collision point.
#
#           Phase diagram of Collision Angle (phi) x Alpha, where
#           alpha is the same as above.
# 
# WHAT UNITS ARE USED?
#   Radians for the angles.
#
# COMMENTS:
#
#   I tried to make the code as easy to read possible, sometimes
#   sacrificing speed, but making it easier to modify and reuse.
#
#   Notation follows http://bit.ly/1zE1gZF
#
# written by Carlos Augusto B. de Gois, 2014
# free to use and modify.

import numpy as np
from geometry import *

# If collision is detected, updates and returns 'vx' and 'vy'
def detectCollision(xx, yy, vx, vy):
    flag = False

    # Is particle inside the right circle?
    if (xx >= width / 2):
        # If particle collides with right circle.
        if np.hypot(xx - circle_x, yy - circle_y) >= radius:
            flag = True

            # Circle point angle, reversed col. angle, refl. angle.
            ca = np.arctan2(yy - circle_y, xx - circle_x)
            rca = np.arctan2(-vy, -vx)
            alpha = rca + (ca - rca) * 2.0
            vx, vy = np.cos(alpha), np.sin(alpha)

    elif (xx <= - width / 2.0):
        # If particle collides with circle to the left.
        if np.hypot(xx + circle_x, yy + circle_y) >= radius:
            flag = True

            # Circle point angle, reversed col. angle, refl. angle.
            ca = np.arctan2(yy + circle_y, xx + circle_x)
            rca = np.arctan2(-vy, -vx)
            alpha = rca + (ca - rca) * 2
            vx, vy = np.cos(alpha), np.sin(alpha)

    # Collides with horizontal walls.
    elif (yy > height / 2.0) or (yy < - height / 2.0):
        flag = True
        vy = -vy

    return flag, vx, vy
