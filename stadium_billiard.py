# This is a simulation for the project "Chaotic Optical Resonators,"
# under research at the Device Research Laboratory (LPD), 
# State University of Campinas, Brazil.
#
# WHAT DOES IT DO?
#   It simulates a stadium billiard system.
#   
#   INPUT:  The rectangle width and height (= semicircle diameter.)
#   
#   OUTPUT: Phase diagram of Arc Length Fraction x Alpha, where 
#           alpha is the reflection angle with respect to the tangent of
#           the collision point.
#
#           Phase diagram of Collision Angle (phi) x Alpha, where
#           alpha is the same as above.
# 
# WHAT UNITS ARE USED?
#   Radians for the angles.
#
# COMMENTS:
#
#   I tried to make the code as easy to read possible, sometimes
#   sacrificing speed, but making it easier to modify and reuse.
#
#   Notation follows http://bit.ly/1zE1gZF
#
# written by Carlos Augusto B. de Gois, 2014
# free to use and modify.

import matplotlib.pyplot as plt
from pylab import * 
import pylab

from boundary import createBoundary
from collision import detectCollision
from geometry import *

##### Functions #####

# Exports plot to .png file
def savePlot(img_count):

    fig = setPlot('Stadium Billiard Trajectory\n\n')
    
    # Background image
    img = imread("/Users/cgois/back.jpg")
    plt.imshow(img, zorder = 0, extent=[-1.5, 1.5, -1.0, 1.0])

    # Plot boundary and lines of past collisions
    plt.scatter(x_bound, y_bound, color='black', s=1, zorder=2)
    plt.plot(xcol_vals_1, ycol_vals_1, color='red', linewidth=1.5, zorder=1)
    plt.plot(xcol_vals_2, ycol_vals_2, color='yellow', linewidth=1.5, zorder=1)

    # Plot line form last collision to current position
    xx1_0, yy1_0 = xcol_vals_1[-1], ycol_vals_1[-1]
    xx2_0, yy2_0 = xcol_vals_2[-1], ycol_vals_2[-1]   
    plt.plot([xx1_0, xx1], [yy1_0, yy1], color='red', linewidth=1.5, zorder=1)
    plt.plot([xx2_0, xx2], [yy2_0, yy2], color='yellow', linewidth=1.5, zorder=1)

    # Some parameters
    plt.ylabel('$y$')
    plt.xlabel('$x$')
    pylab.ylim([-1,1])
    pylab.xlim([-1.5,1.5])

    # Export and erase plot
    plt.savefig(str(img_count) + '.png', dpi=300) 
    close('all')
#    print 'img {} exported' .format(img_count)

def setPlot(title = 'Unnamed'):
    # Create plot figure, set title and params.
    fig = plt.figure(figsize = (11, 8))
    plt.title(title)
    plt.grid()

##### Main #####


# Arrays for storing collision coordinates of both particles.
xcol_vals_1 = np.zeros(1, dtype=np.float64)
ycol_vals_1 = np.zeros(1, dtype=np.float64)
xcol_vals_2 = np.zeros(1, dtype=np.float64)
ycol_vals_2 = np.zeros(1, dtype=np.float64)

x_bound = np.array([], dtype=np.float64)
y_bound = np.array([], dtype=np.float64)

# nof collisions and time;
cols, tot_cols, img_count = 0, 100, 0
tt = 0.

# Particle 1 initial parameters
phi = np.pi / 3
vx1, vy1 = np.cos(phi), np.sin(phi)
xx1, yy1 = 0.0, 0.0

# Particle 2 initial parameters
phi2 = 1.01 * phi
vx2, vy2 = np.cos(phi2), np.sin(phi2)
xx2, yy2 = 0.0, 0.0

x_bound, y_bound = createBoundary()
while cols < tot_cols:

    savePlot(img_count)
    img_count += 1

    xx1 += t_step * vx1
    yy1 += t_step * vy1 
    
    xx2 += t_step * vx2
    yy2 += t_step * vy2

    flag1, vx, vy = detectCollision(xx1, yy1, vx1, vy1)
    if flag1:
        xcol_vals_1 = np.append(xcol_vals_1, xx1)
        ycol_vals_1 = np.append(ycol_vals_1, yy1)
        vx1, vy1 = vx, vy
    
    flag2, vx, vy = detectCollision(xx2, yy2, vx2, vy2)
    if flag2:
        xcol_vals_2 = np.append(xcol_vals_2, xx2)
        ycol_vals_2 = np.append(ycol_vals_2, yy2)
        vx2, vy2 = vx, vy
