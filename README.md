Plots the trajectory of two particles in a stadium billiard, and save image files of each step. 
The images can be compiled into a video, using ffmpeg or anyting similar. 
Slightly different initial directions for the particles gives an illustration of chaotic dynamics.

Included files:

- **boundary.py:** auxiliary function to define the stadium boundaries.
- **collision.py:** auxiliary function to detect particle-boundary collisions.
- **geometry.py:** auxiliary file to set geometry parameters.
- **stadium_billiard.py:** main file to create boundary, propagate particles, detect collisions, 
plot current state, and save to image file.

Code is not well documented, but should be easy enough to follow. 
Also, no optimisation was made, for I needed a single run. 
Generated video is included.