# This is a simulation for the project "Chaotic Optical Resonators,"
# under research at the Device Research Laboratory (LPD), 
# State University of Campinas, Brazil.
#
# WHAT DOES IT DO?
#   It simulates a stadium billiard system.
#   
#   INPUT:  The rectangle width and height (= semicircle diameter.)
#   
#   OUTPUT: Phase diagram of Arc Length Fraction x Alpha, where 
#           alpha is the reflection angle with respect to the tangent of
#           the collision point.
#
#           Phase diagram of Collision Angle (phi) x Alpha, where
#           alpha is the same as above.
# 
# WHAT UNITS ARE USED?
#   Radians for the angles.
#
# COMMENTS:
#
#   I tried to make the code as easy to read possible, sometimes
#   sacrificing speed, but making it easier to modify and reuse.
#
#   Notation follows http://bit.ly/1zE1gZF
#
# written by Carlos Augusto B. de Gois, 2014
# free to use and modify.

from pylab import * 
from geometry import *

def createBoundary(tot_cols=10000):

    # Arrays for storing collision coordinates and other data.
    xcol_vals = np.array([], dtype=np.float64)
    ycol_vals = np.array([], dtype=np.float64)
    
    phi = np.pi * np.random.random()
    vx, vy = np.cos(phi), np.sin(phi)
    xx, yy = 0.0, 0.0
    cols = 0

    while cols < tot_cols:

        xx += t_step * vx
        yy += t_step * vy
   
        # Is particle inside the right circle?
        if (xx >= width / 2):
            # If particle collides with right circle.
            if np.hypot(xx - circle_x, yy - circle_y) >= radius:
                # Circle point angle, reversed col. angle, refl. angle.
                ca = np.arctan2(yy - circle_y, xx - circle_x)
                rca = np.arctan2(-vy, -vx)
                alpha = rca + (ca - rca) * 2.0
                vx, vy = np.cos(alpha), np.sin(alpha)
            else:
                continue

        elif (xx <= - width / 2.0):
            # If particle collides with circle to the left.
            if np.hypot(xx + circle_x, yy + circle_y) >= radius:
                # Circle point angle, reversed col. angle, refl. angle.
                ca = np.arctan2(yy + circle_y, xx + circle_x)
                rca = np.arctan2(-vy, -vx)
                alpha = rca + (ca - rca) * 2
                vx, vy = np.cos(alpha), np.sin(alpha)
            else:
                continue

        # Collides with horizontal walls.
        elif (yy > height / 2.0) or (yy < - height / 2.0):
            vy = -vy
        # If didnt collide, back to start of the loop.
        else:
            continue

        cols += 1

        # Store collision point.
        xcol_vals = np.append(xcol_vals, xx)
        ycol_vals = np.append(ycol_vals, yy) 

    print 'boundary ok'
    return xcol_vals, ycol_vals
